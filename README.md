# Tweetoscope project

This project was developped as part of the Software Application Engineering class in our final year as Data and Information Science students in CentraleSupélec. 

The aim of the project is to predict the popularity of tweets generated through a c++ program mimicking real life tweets. A popularity of a tweet is defined as the number of times this tweet will be retweeted, i.e. forwarded by users to their followers. Because retweets propagate with a cascade effect, a tweet and all retweets it triggered, build up what is hereafter called a cascade. Once we are able to predict popularities of tweets, it is straight-forward to identify the most promising tweets with the highest expected popularity. So to summarize, the problem is to guess the final size of cascades, just by observing the beginning of these cascade during a given observation time window, like, say, the ten first minutes.

For the sake of simplification, our answer only takes into account the times (since the original tweet to be retweeted) and magnitudes (number of followers of each person that tweeted or retweeted) of each tweet and doesn't take into account the  content of the messages of tweets and retweets or other complex factors.

The cascades of tweets can be simulated as a Hawkes process. 
We use MAP estimators and random forests to estimate popularities of tweets in this project.

In order to design an elastic distributed application able to be deployed on the Cloud and to scale seamlessly so that it can process an arbitrarily large number of tweets, we have used tools such as Kafka, Docker and Kubernetes.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

In order to use our application, you will need Minikube.

Follow the appropriate documentation for your operating system to install [Minikube](https://minikube.sigs.k8s.io/docs/start/)


### Installing

You will only need to download the YamlFiles directory on your local machine. 


Start minikube and launch zookeeper and kafka services :

```
minikube start

kubectl apply -f zookeeper-and-kafka.yml

```

For V0 of the application (Tweet generation only) or V2 (Generation + collection + prediction)

```

kubectl apply -f tweetoscope-deploy-V0.yml

kubectl apply -f tweetoscope-deploy-V2.yml

```

You can check your pods status and their logs on minikube 


```
kubectl get pods

kubectl logs name_or_ID_of_the_pod

```

See the ouput files 

```
minikube ssh

#For V0 : 

cd /tmp/generatorVol
cat stats_generator.txt

#For V2 :

cd /tmp/predictor-vol-300
cat ARE.txt

```


## Deployment

You can also deploy our application on the InterCell cluster using the appropriate YamlFiles (cpusdi1_8).

## Built With

* [APACHE KAFKA](https://kafka.apache.org/) -  Stream-processing 
* [Docker](https://maven.apache.org/) - OS-level virtualization to create containers
* [Kubernetes](https://kubernetes.io/) - Automated container deployment, scaling, and management

## Authors

* **Mélina Benahmed**
* **Nina Achache**
* **Zineb El Bachiri**

## Acknowledgments

* Our professors who designed and made this project possible
* Hat tip to anyone whose code or youtube video helped us with our project
