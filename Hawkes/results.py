import pandas as pd

#reading the txt files

#define the columns
my_cols_are = ['casade','cid','T_obs:','T_obs','ARE:','ARE','predicted_final_size:','predicted_final_size','true_final_size:','true_final_size','G1:','G1']  # create some col names
my_cols_pred = ['casade','cid','final size','final_size']  

#splitting the lines according to the columns
df_pred = pd.read_csv("pred.txt",sep=",|:",names=my_cols_pred, header=None, engine="python")
df_are = pd.read_csv("ARE.txt",sep=",|:",names=my_cols_are, header=None, engine="python")

#filtering on relevant columns
pred=df_pred.filter(['cid','final_size'],axis=1)
are=df_are.filter(['cid','T_obs','ARE','predicted_final_size','true_final_size','G1'],axis=1)

#compute medians
median_ARE_300=are.loc[are['T_obs']==300,['ARE']].median()
median_ARE_600=are.loc[are['T_obs']==600,['ARE']].median()
median_ARE_1200=are.loc[are['T_obs']==1200,['ARE']].median()

print("Our model for T_obs = 300 has a median "+ str(median_ARE_300))
print("Our model for T_obs = 300 has a median "+ str(median_ARE_600))
print("Our model for T_obs = 300 has a median "+ str(median_ARE_1200))
print("!!!WARNING!!! We choosed to calculate the median rather than the mean because we have odd values")
