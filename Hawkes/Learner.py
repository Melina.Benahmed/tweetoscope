#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 15:55:27 2020

@author: Nina, Mélina & Zineb 

"""

# External Import 
import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producder
from kafka import TopicPartition
from sklearn.ensemble import RandomForestRegressor
import pickle

# Internal Import
import logger

###########################################################################
#################### Defining producers and consumers #####################
###########################################################################

# Topics's name

samples_topic="samples"  # Reading from
models_topic="models" # Writing to

#topics'key
#key_dic ={"300":0, "600":1, "1200":2}


# Arguments to write to run the file in a terminal  "python3 Hawkes --broker-list localhost:9092 --obs-wind 300".
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
parser.add_argument('--obs-wind', type=str,required=True, help="the observation window : 300/600/1200") # In order to // the calculs. 
args = parser.parse_args()  # Parse arguments

# Consumer
consumer_samples = KafkaConsumer( samples_topic,                                       
                          bootstrap_servers = args.broker_list,                        
                          value_deserializer =lambda v: json.loads(v.decode('utf-8')), 
                          key_deserializer = lambda v: v.decode(),                  
                          auto_offset_reset="earliest",                               
                          group_id = "SamplesConsumerGroup-{}".format(args.obs_wind))


#consumer_samples.assign([TopicPartition(samples_topic, key_dic[args.obs_wind])])

# Producer
producer_models = KafkaProducer(bootstrap_servers = args.broker_list,        # List of brokers passed from the command line
                          value_serializer=lambda m: pickle.dumps(m)         # How to serialize the value to a binary buffer using pickle this time because we send a forest not a message.
                          )

###########################################################################
############################## Random Forest ##############################
###########################################################################

# To better follow the messages sent to the topic samples, we save them in a text file.
samples=open('samples.txt','a')# Argument a is to write and create if needed the file

X=[] # List of parameters [beta,n_etoile,G1].
W=[] # List of true omegas.

L_when_to_send=[1,5,10,20,50,100] # We compute a random forest when 1/5/10/20/50/100 samples are received or every 100 samples received.

logger = logger.get_logger('Learner', broker_list=args.broker_list, debug=True)  # Identify the node of origin of the message.
 
# Reading samples topic.
for msg in consumer_samples:
    
    # Getting the data from msg
    T_obs=msg.key
    msg=msg.value
    X.append(msg['X'])
    w=msg['w']
    W.append(w)
    
    logger.debug("Sample received." + str(T_obs)) # Communicating that we correctly reads samples topic.
    samples.write(('{cascade:'+str(msg['cid'])+', T_obs: '+str(T_obs)+' , w: '+str(w)+'\n')) # Saving the message in the text file.
    
    if len(X) in L_when_to_send or len(X)%100==0 : 
        logger.debug("New forest is calculated." + str(T_obs))
        
        regr= RandomForestRegressor()# We compute a new forest.
        forest = regr.fit(X,W)

        logger.info("Sending messages to models." + str(T_obs))
        producer_models.send(models_topic, forest) # Sending the new forest as a pickle.
producer_models.flush() 
