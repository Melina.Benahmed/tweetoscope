/*

  The boost library has clever tools for handling program
  parameters. Here, for the sake of code simplification, we use a
  custom class.

*/




#pragma once

#include <tuple>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstddef>
#include <stdexcept>
#include <map>
#include <memory>
#include <queue>
#include <iomanip>
#include <boost/heap/binomial_heap.hpp>

//partie pour le parsing

namespace tweetoscope {
  namespace params {
    namespace section {
      struct Kafka {
	      std::string brokers;
      };
      
      struct Topic {
	      std::string in, out_series, out_properties;
      };
      
      struct Times {
        std::vector<std::size_t> observation;
        std::size_t              terminated;
      };

      struct Cascade {
        std::size_t min_cascade_size;
        };
    }
    
    struct collector {
    private:
      std::string current_section;

      std::pair<std::string, std::string> parse_value(std::istream& is) {
        char c;
        std::string buf;
        is >> std::ws >> c;
        while(c == '#' || c == '[') {
          if(c == '[') std::getline(is, current_section, ']');
          std::getline(is, buf, '\n');
          is >> std::ws >> c;
        }
        is.putback(c);
        std::string key, val;
        is >> std::ws;
        std::getline(is, key, '=');
        is >> val;
        std::getline(is, buf, '\n');
        return {key, val};
      }
      
    public:

      section::Kafka   kafka;
      section::Topic   topic;
      section::Times   times;
      section::Cascade cascade;

      collector(const std::string& config_filename) {
        std::ifstream ifs(config_filename.c_str());
        if(!ifs)
          throw std::runtime_error(std::string("Cannot open \"") + config_filename + "\" for reading parameters.");
        ifs.exceptions(std::ios::failbit | std::ios::badbit | std::ios::eofbit);
        try {
          while(true) {
            auto [key, val] = parse_value(ifs);
            if(current_section == "kafka") {
              if(key == "brokers") kafka.brokers = val;
            }
            else if(current_section == "topic") {
              if     (key == "in")             topic.in             = val;
              else if(key == "out_series")     topic.out_series     = val;
              else if(key == "out_properties") topic.out_properties = val;
            }
            else if(current_section == "times") {
              if     (key == "observation")    times.observation.push_back(std::stoul(val));
              else if(key == "terminated")     times.terminated = std::stoul(val);
            }
            else if(current_section == "cascade") {
              if (key == "min_cascade_size")   cascade.min_cascade_size = std::stoul(val);
            }
          }
        }
        catch(const std::exception& e) {/* nope, end of file occurred. */}
      }
    };

    inline std::ostream& operator<<(std::ostream& os, const collector& c) {
      os  << "[kafka]" << std::endl
          << "  brokers=" << c.kafka.brokers << std::endl
          << std::endl
          << "[topic]" << std::endl
          << "  in=" << c.topic.in << std::endl
          << "  out_series=" << c.topic.out_series << std::endl
          << "  out_properties=" << c.topic.out_properties << std::endl
          << std::endl
          << "[times]" << std::endl;
      for(auto& o : c.times.observation){
          os << "  observation=" << o << std::endl;
          os << "  terminated=" << c.times.terminated << std::endl
             << std::endl
             << "[cascade]" << std::endl
             << "  min_cascade_size=" << c.cascade.min_cascade_size << std::endl;
      };
        return os;
    }
  }

  using timestamp = std::size_t;

  namespace source {
    using idf = std::size_t;
  }
  
  struct tweet {
    std::string type = "";
    std::string msg  = "";
    timestamp time   = 0;
    double magnitude = 0;
    source::idf source = 0;
    std::string info = "";
  };

  namespace cascade {

    class Processor;
    class Cascade;

    using refCascade = std::shared_ptr<Cascade>;
    using weakCascade = std::weak_ptr<Cascade>;
    using properties = std::vector<std::string>;
    using series = std::vector<std::string>;
    using idf = std::size_t;

    struct refCascade_comparator {
      //predicate
      bool operator()(refCascade cscd1, refCascade cscd2) const{
        return cscd1 > cscd2;
      };
    };

    using priority_queue = boost::heap::binomial_heap<refCascade,boost::heap::compare<refCascade_comparator> >;
    using time_magnitude = std::vector<std::pair<timestamp,int>>;

    std::ostream& operator<<(std::ostream& os, time_magnitude& tm){
        os<< "[" ;
        auto tm_iterator = tm.begin();
        while(tm_iterator != tm.end()-1){
          os<<" [" << tm_iterator->first << ',' << tm_iterator->second << "] ,";
          ++tm_iterator;
        }
        os<<" [" << tm_iterator->first << ',' << tm_iterator->second << "] ";
        os<< "]";
        return os;
    };
    
   
    class Cascade{  
      private:
      std::string cid;
      std::string msg="";
      timestamp time_first_twt;
      timestamp time_last_twt;
      time_magnitude times_magnitudes;
      source::idf source;

      public:

      friend class Processor;
      friend struct refCascade_comparator;
      friend  std::ostream& operator<<(std::ostream& os,time_magnitude& tm);



      //Constructeur
      Cascade(tweet& twt, std::string& key){
        cid = key;
        msg = twt.msg;
        time_first_twt = twt.time;
        time_last_twt = twt.time;
        times_magnitudes = {std::make_pair(twt.time,twt.magnitude)};
        source = twt.source;
      };

      //Destructeur  
      ~Cascade(){};
        
      void cascade_update(tweet& twt, std::string& key){
        times_magnitudes.push_back(std::make_pair(twt.time,twt.magnitude));
        time_last_twt = twt.time;         
      };

    bool operator<(const refCascade& other) const {
        return time_last_twt > other->time_last_twt;}
    };

    // This is a convenient function for pointer allocation.
    refCascade cascade_ptr(tweet& twt, std::string& key) {return std::make_shared<Cascade>(twt, key);};


    class Processor {

      public:

      source::idf source;
      timestamp source_time;
      priority_queue priorityqueue;
      std::map<timestamp,std::queue<weakCascade>> fifo;


      friend class Cascade;
      friend std::ostream& operator<<(std::ostream& os,tweetoscope::cascade::time_magnitude& tm);
      std::map<std::string,weakCascade> symbole_table;

      
      Processor(tweet& twt) : priorityqueue{}, fifo{},symbole_table{} {
        source =twt.source;
        source_time=twt.time;
      };
      Processor(const Processor& cpy) =default;
      Processor(Processor&& cpy) =default;
      Processor& operator=(const Processor& cpy)=default;
      Processor& operator=(Processor&& cpy)=default;
      ~Processor(){};

      series send_partial_cascade(std::vector<std::size_t> observation){
        series to_send;
        for(auto T_obs : observation){
            std::vector<std::string> cid_to_send;
            if(!fifo[T_obs].empty()){
                weakCascade wk_cascade = fifo[T_obs].front();  // Takes the first element of the pile (celui qui a été push en tt premier)
                auto cscd = wk_cascade.lock(); // C SUR ET CERTAIN que le sharedptr existe dans priorityqueue vu qu'on traitr dabord les partial cascade avant les terminal cascades ou on les fait enlever des priority queus :)
                //check the window time condition
                while((source_time - cscd->time_first_twt)>T_obs){
                    time_magnitude tm_to_send;
                    auto tm_iterator = cscd->times_magnitudes.begin();
                    while((tm_iterator->first -cscd->time_first_twt <= T_obs) & (tm_iterator!=cscd->times_magnitudes.end())){
                      tm_to_send.push_back(*tm_iterator);
                      ++tm_iterator; //++i is faster than i++
                    }
                    std::ostringstream ostr_series;
                    ostr_series << "{"
                                << "\"type\" : \"serie\""
                                << ", \"cid\" : " << cscd->cid
                                << ", \"msg\": \"" << cscd->msg  << '"'
                                << ", \"T_obs\" : " << T_obs
                                << ",\"tweets\" :" << tm_to_send
                                << '}';
                    std::string msg_series = ostr_series.str();
                    if (std::count( cid_to_send.begin(), cid_to_send.end(),cscd->cid)){
                        std::cout << "Duplicated key : "<< cscd->cid << " , T_obs : "<< T_obs << std::endl;
                    }
                    else{
                      to_send.push_back(msg_series);
                      cid_to_send.push_back(cscd->cid);
                    }
                    fifo[T_obs].pop(); //REMOVE THE FIRST ELEMENT
                    if(!fifo[T_obs].empty()){
                      wk_cascade = fifo[T_obs].front();
                      auto cscd = wk_cascade.lock();
                    }
                    else{
                      break;
                    }
                }
            }
        }    
        return to_send;
      };


      properties send_terminated_cascade(timestamp& T_over, std::size_t minimum_size){
        properties to_send;
        if (!priorityqueue.empty()){
          auto cscd = priorityqueue.top();
          while(source_time-cscd->time_last_twt>T_over){
            if (cscd->times_magnitudes.size() > minimum_size){
            std::ostringstream ostr_properties;
            ostr_properties << "{"
                            << "\"type\" : \"size\""
                            << ", \"cid\" : " << cscd->cid
                            << ", \"n_tot\" :" << cscd->times_magnitudes.size()
                            << ", \"t_end\":" << cscd->time_last_twt
                            << '}';
            std::string msg_properties = ostr_properties.str();
            to_send.push_back(msg_properties);
            }
            priorityqueue.pop();
            if (!priorityqueue.empty()){
            cscd = priorityqueue.top();
                      }
            else{
              break;
            }
          }
        }    
        return to_send;
      };
    };
  };
 
  inline std::string get_string_val(std::istream& is) {
    char c;
    is >> c; // eats  "
    std::string value;
    std::getline(is, value, '"'); // eats tweet", but value has tweet
    return value; 
  }

  inline std::istream& operator>>(std::istream& is, tweet& t) {
    // A tweet is  : {"type" : "tweet"|"retweet", 
    //                "msg": "...", 
    //                "time": timestamp,
    //                "magnitude": 1085.0,
    //                "source": 0,
    //                "info": "blabla"}
    std::string buf;
    char c;
    is >> c; // eats '{'
    is >> c; // eats '"'
    while(c != '}') { 
      std::string tag;
      std::getline(is, tag, '"'); // Eats until next ", that is eaten but not stored into tag.
      is >> c;  // eats ":"
      if     (tag == "type")    t.type = get_string_val(is);
      else if(tag == "msg")     t.msg  = get_string_val(is);
      else if(tag == "info")    t.info = get_string_val(is);
      else if(tag == "t")       is >> t.time;
      else if(tag == "m")       is >> t.magnitude;
      else if(tag == "source")  is >> t.source;
            
      is >> c; // eats either } or ,
      if(c == ',')
        is >> c; // eats '"'
    } 
    return is;
  }


}