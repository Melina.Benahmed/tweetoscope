#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Nina, Mélina & Zineb

Writing a stats text to use in V0 
"""



# External Import 
import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer

""" Reading from the topic tweets that has been generated"""



# Topics's name
topic_lecture="tweets" 

# Arguments to write to run the file in a terminal  "python3 stats_generator.py --broker-list localhost:9092".
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
args = parser.parse_args()  # Parse arguments


###########################################################################
########## Creating the text file and reading from tweets topic ###########
###########################################################################



#Creating the consumer + Reading JSON Message
consumer_tweets = KafkaConsumer(topic_lecture,                       
                          bootstrap_servers = args.broker_list,                       
                          value_deserializer =lambda v: json.loads(v.decode('utf-8')),                      
                          auto_offset_reset="earliest",
                          group_id = "myOwnP16",
                          consumer_timeout_ms = 1000)

L_keys = []
L_msg=[]

f = open('/tmp/stats_generator.txt',"a+")
i=0
for msg in consumer_tweets:
    L_keys.append(msg.key)
    L_msg.append(msg.value)
    #print('Reading messages here')

x = "Number of keys : "+ str(len(L_keys)) + ", Number of messages : " + str(len(L_msg)) +"\n"
f.write(x ) 

print("Finished writing in the file, bye !")
